# README #

## Summary ##

Provides a Dockerfile for the apache-dispatch router project built atop the latest version of CentOS. 

Due to specific requirement, the image will also, when built, build qpid-proton.

## Pre-Requistes ##

You must bring your own qdrouterd.conf file for the best experience.

## How to Run ##

Assuming you have pulled the image from hub.docker.com.

$ docker run -p 5672:5672 -d -v <SOURCE_DIR/qdrouterd.conf>:/home/dispatch/qdrouterd.conf elsammons/centos-apache-dispatch